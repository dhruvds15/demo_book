class BooksController < ApplicationController
	def index
		@books = Book.all
		render json: @books, status: :ok
	end

	def create
		@book = Book.new(create_book_params)

		if @book.save
			render json: @book, status: :created
		else
			render json: {errors: @book.errors.full_messages}, status: :unprocessable_entity
		end
	end

	def show
		begin
			@book = Book.find(params[:id])
			render json: @book, status: :ok
		rescue => e
			render json: {error: "Book not found"}, status: :not_found
		end
	end

	def get_book_by_title
		@book = Book.where(title: params[:book][:title]).first
		if @book.present?
			render json: @book, status: :ok
		else
			render json: {error: "Book not found"}, status: :not_found
		end
	end

	private

	def create_book_params
		params.require(:book).permit(:title, :author)
	end

	def book_param
		params.require(:book).permit(:title)
	end
end
