class Book < ApplicationRecord
  validates :title, presence: true, uniqueness: true

  belongs_to :author
end
