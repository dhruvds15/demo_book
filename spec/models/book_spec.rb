require 'rails_helper'

RSpec.describe Book, type: :model do
  # pending "add some examples to (or delete) #{__FILE__}"
  context 'validation tests' do
    it 'ensures title should be present' do
      book = Book.new(author: "test").save

      expect(book).to eq(false)
    end

    it 'should save successfully' do
      book = Book.new(title: "Title", author: "Author").save

      expect(book).to eq(true)
    end
  end
end
