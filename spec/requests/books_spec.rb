require 'rails_helper'

describe 'Books API', type: :request do
  context 'index api test' do
    it 'should have successful response' do
      get '/books'
      expect(response).to have_http_status(:success)
    end
  
    it 'returns all books in json format' do
      book1 = FactoryBot.create(:book , title: 'Book1', author: 'Author1')
      book2 = FactoryBot.create(:book , title: 'Book2', author: 'Author2')
      get '/books'
      
      json_response = JSON.parse(response.body)
      
      expect(json_response).to be_an(Array)
      expect(json_response.length).to eq(2)
      expect(json_response[0]['title']).to eq(book1.title)
      expect(json_response[0]['author']).to eq(book1.author)
      expect(json_response[1]['title']).to eq(book2.title)
      expect(json_response[1]['author']).to eq(book2.author)
    end  
  end

  context 'show api test'do
    it 'returns the book with successfull response' do
      book = FactoryBot.create(:book, title: 'Book1', author: 'Author1')
      get "/books/#{book.id}"
      expect(response).to have_http_status(:success)
    end

    it 'returns book in json format' do
      book = FactoryBot.create(:book, title: 'Book1', author: 'Author 1')
      get "/books/#{book.id}"
      json_response = JSON.parse(response.body)
      expect(json_response).to be_a(Hash)
      expect(json_response['title']).to eq(book.title)
      expect(json_response['author']).to eq(book.author)
    end

    context 'if book does not exist' do
      it 'should return an error with status 404(not found)' do
        get "/books/invalid_id"

        expect(response).to have_http_status(:not_found)
        json_response = JSON.parse(response.body)
        expect(json_response).to include('error' => 'Book not found')
      end
    end
  end

  context 'Fetch book details by title' do
    context 'When Book exists' do
      it 'should return book with successful response' do
        book = FactoryBot.create(:book, title: 'Book1', author: 'Author1')
  
        get '/books/get_book_by_title', params: {book: {title: 'Book1'}}
        
        expect(response).to have_http_status(:success)
  
        json_response = JSON.parse(response.body)
  
        expect(json_response).to be_a(Hash)
        expect(json_response['title']).to eq(book.title)
      end
    end

    context 'When Book does not exist' do
      it 'should return an error with status 404' do
        get '/books/get_book_by_title', params: {book: {title: 'Invalid Title'}}
        expect(response).to have_http_status(:not_found)

        json_response = JSON.parse(response.body)
        expect(json_response).to include('error' => 'Book not found')
      end
    end
  end
  context 'Create Book API' do
    context 'When Book is created' do
      it 'should create book with created response' do
        book_attr = {title: 'Harry Potter', author: 'J.K Rowling'}
        post '/books', params: {book: book_attr}

        expect(response).to have_http_status(:created)
        json_response = JSON.parse(response.body)
        expect(json_response).to be_a(Hash)
        expect(json_response['title']).to eq(book_attr[:title])
        expect(json_response['author']).to eq(book_attr[:author])
        expect(Book.find_by(title: book_attr[:title], author: book_attr[:author])).not_to be_nil
      end
    end

    context 'When Book is not created'do
      it 'returns an error with status 422' do
        invalid_attr = {title: '', author: ''}
        post '/books', params: {book: invalid_attr}

        expect(response).to have_http_status(:unprocessable_entity)

        json_response = JSON.parse(response.body)
        expect(json_response).to include('errors')
      end
    end
  end
end
