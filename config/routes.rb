Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  resources :books, only: [:index, :create, :show, :destroy] do
    collection do
      get 'get_book_by_title', to: 'books#get_book_by_title'
    end
  end
end
